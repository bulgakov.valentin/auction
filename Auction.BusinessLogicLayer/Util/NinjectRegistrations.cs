﻿using Auction.BusinessLogicLayer.Interfaces;
using Auction.BusinessLogicLayer.Repositories;
using Ninject.Modules;

namespace Auction.BusinessLogicLayer.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWorkIdentity>().To<UnitOfWorkIdentity>().WithConstructorArgument("connectionString", "IdentityForAuction");
            Bind<IUnitOfWorkAuction>().To<UnitOfWorkAuction>().WithConstructorArgument("connectionString", "AuctionDb");
        }
    }
}
