﻿using System;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.Models;

namespace Auction.BusinessLogicLayer.Repositories
{
    public class UnitOfWorkIdentity : IUnitOfWorkIdentity
    {
        private UsersDbContext _usersDb;
        private UserRepository userRepository;
        private RoleRepository roleRepository;

        public UnitOfWorkIdentity(string connectionString)
        {
            _usersDb = new UsersDbContext(connectionString);
        }

        public IUserRepository<ApplicationUser> Users
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(_usersDb);
                }

                return userRepository;
            }
        }

        public IRoleRepository<ApplicationRole> Roles
        {
            get
            {
                if (roleRepository == null)
                {
                    roleRepository = new RoleRepository(_usersDb);
                }

                return roleRepository;
            }
        }

        public void Save()
        {
            _usersDb.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _usersDb.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
