﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Auction.BusinessLogicLayer.Identity;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Identity;
using Auction.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace Auction.BusinessLogicLayer.Repositories
{
    class RoleRepository : IRoleRepository<ApplicationRole>
    {
        private UsersDbContext _dbContext;
        public AppRoleManager RoleManager => HttpContext.Current.GetOwinContext().GetUserManager<AppRoleManager>();
        public AppUserManager UserManager => HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();

        public RoleRepository(UsersDbContext context)
        {
            _dbContext = context;
        }

        public async Task<bool> Create(ApplicationRole role)
        {
            IdentityResult result = await RoleManager.CreateAsync(role);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<ApplicationRole> GetAll()
        {
            return RoleManager.Roles.ToList();
        }

        public async Task<ApplicationRole> Get(string id)
        {
            return await RoleManager.FindByIdAsync(id);
        }
        public IEnumerable<ApplicationRole> Find(Func<ApplicationRole, bool> predicate)
        {
            return RoleManager.Roles.AsNoTracking().AsEnumerable().Where(predicate).ToList();
        }

        public void Update(ApplicationRole role)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> Delete(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await RoleManager.DeleteAsync(role);
                if (result.Succeeded)
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}
