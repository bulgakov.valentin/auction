﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Auction.BusinessLogicLayer.Identity;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Identity;
using Auction.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Auction.BusinessLogicLayer.Repositories
{
    public class UserRepository : IUserRepository<ApplicationUser>
    {
        private UsersDbContext _dbUsers;
        //private UserManager<ApplicationUser> userManager;
        public UserRepository(UsersDbContext usersDbContext)
        {
            this._dbUsers = usersDbContext;
            //userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(usersDbContext));
        }
        public AppUserManager UserManager => HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();
        public AppRoleManager RoleManager => HttpContext.Current.GetOwinContext().GetUserManager<AppRoleManager>();

        public async Task<bool> Create(ApplicationUser user, string parameter)
        {

            IdentityResult result = await UserManager.CreateAsync(user, parameter);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            return UserManager.Users.ToList();
            //return _dbUsers.Users.ToList();
        }

        public async Task<ApplicationUser> Get(string id)
        {
            return await UserManager.FindByIdAsync(id);
            //return _dbUsers.Users.Find(id);
        }

        public IEnumerable<ApplicationUser> Find(Func<ApplicationUser, bool> predicate)
        {
            return UserManager.Users.AsEnumerable().Where(predicate).ToList();
        }

        public void Update(ApplicationUser user)
        {
            _dbUsers.Entry(user).State = EntityState.Modified;
        }

        public bool Delete(string id)
        {
            ApplicationUser user = _dbUsers.Users.Find(id);
            if (user!=null)
            {
                _dbUsers.Users.Remove(user);
                _dbUsers.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
