﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auction.BusinessLogicLayer.AuctionData;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Contexts;

namespace Auction.BusinessLogicLayer.Repositories
{
    public class UnitOfWorkAuction : IUnitOfWorkAuction
    {
        private AuctionModels _db;

        private CategoryData categories;
        private SubCategoryData subCategories;
        private ProductData products;
        //public ISubCategory subCategories;

        public UnitOfWorkAuction(string connectionString)
        {
            _db = new AuctionModels(connectionString);
        }

        public ICategory Categories
        {
            get
            {
                if (categories == null)
                {
                    categories = new CategoryData(_db);
                }

                return categories;
            }
        }

        public ISubCategory SubCategories
        {
            get
            {
                if (subCategories==null)
                {
                    subCategories = new SubCategoryData(_db);
                }

                return subCategories;
            }
        }

        public IProduct Products
        {
            get
            {
                if (products==null)
                {
                    products = new ProductData(_db);
                }

                return products;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
