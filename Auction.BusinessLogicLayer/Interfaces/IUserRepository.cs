﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auction.DataAccessLayer.Entities.Identity;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface IUserRepository<TEntity> where TEntity : class
    {
        AppUserManager UserManager { get; }
        AppRoleManager RoleManager { get; }
        Task<bool> Create(TEntity user, string parameter); 
        IEnumerable<TEntity> GetAll();
        Task<TEntity> Get(string id);
        IEnumerable<TEntity> Find(Func<TEntity, Boolean> predicate);
        void Update(TEntity user);
        bool Delete(string id);
    }
}
