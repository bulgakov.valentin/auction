﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Auction.DataAccessLayer.Entities.Shop;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface ICategory
    {
        IEnumerable<Category> GetAll();
        IEnumerable<Category> GetRange(int first, int last);
        Category GetById(int categoryId);
        bool RemoveById(int categoryId);
        bool AddCategory(Category category, HttpPostedFileBase file);
        bool EditCategory(int categoryId, Category category);
        bool CheckSame(Category category);
        string WriteImage(HttpPostedFileBase file, string name);
        IEnumerable<Category> Find(Func<Category, Boolean> predicate);

        bool ChangeImage(int categoryId, string path);
    }
}
