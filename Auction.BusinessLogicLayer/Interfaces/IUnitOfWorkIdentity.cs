﻿using System;
using Auction.Models;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface IUnitOfWorkIdentity : IDisposable
    {
        IUserRepository<ApplicationUser> Users { get; }
        IRoleRepository<ApplicationRole> Roles { get; }
        void Save();
    }
}
