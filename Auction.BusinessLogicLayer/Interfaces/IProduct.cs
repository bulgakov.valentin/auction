﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Auction.DataAccessLayer.Entities.Shop;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface IProduct
    {
        IEnumerable<Product> GetAll();
        IEnumerable<Product> GetRange(int first, int last);
        Product GetById(int productId);
        bool RemoveById(int productId);
        bool AddProduct(Product product, HttpPostedFileBase file);
        bool EditProduct(int productId, Product product);
        bool CheckSame(Product product);
        string WriteImage(HttpPostedFileBase file, string name);
        IEnumerable<Product> Find(Func<Product, Boolean> predicate);

        bool ChangeImage(int productId, string path);
    }
}
