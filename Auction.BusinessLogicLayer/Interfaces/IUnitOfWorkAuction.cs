﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface IUnitOfWorkAuction : IDisposable
    {
        ICategory Categories { get; }
        ISubCategory SubCategories { get; }
        IProduct Products { get; }
        //ISubCategory SubCategories { get; }
        void Save();
    }
}
