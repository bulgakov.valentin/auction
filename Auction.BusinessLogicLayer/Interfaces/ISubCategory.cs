﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Auction.DataAccessLayer.Entities.Shop;

namespace Auction.BusinessLogicLayer.Interfaces
{
    public interface ISubCategory
    {
        IEnumerable<SubCategory> GetAll();
        IEnumerable<SubCategory> GetRange(int first, int last);
        SubCategory GetById(int subCategoryId);
        bool RemoveById(int subCategoryId);
        bool AddSubCategory(SubCategory subCategory, HttpPostedFileBase file);
        bool EditSubCategory(int subCategoryId, SubCategory subCategory);
        bool CheckSame(SubCategory subCategory);
        string WriteImage(HttpPostedFileBase file, string name);
        IEnumerable<SubCategory> Find(Func<SubCategory, Boolean> predicate);

        bool ChangeImage(int subCategoryId, string path);
    }
}
