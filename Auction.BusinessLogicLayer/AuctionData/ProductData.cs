using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Contexts;
using Auction.DataAccessLayer.Entities.Shop;

namespace Auction.BusinessLogicLayer.AuctionData
{
    public class ProductData : IProduct
    {
        private AuctionModels _db;

        public ProductData(AuctionModels db)
        {
            _db = db;
        }

        public IEnumerable<Product> GetAll()
        {
            return _db.Products.OrderBy(x => x.DateAdded).ToList();
        }

        public IEnumerable<Product> GetRange(int first, int last)
        {
            int count = _db.Products.Count();
            if (count >= last && first <= count && first >= 0 && last >= first)
            {
                var result = _db.Products.OrderBy(x => x.DateAdded).Skip(first).Take(last).ToList();
                return result;
            }
            else
            {
                if (first >= 0)
                {
                    last = last - (count - first);
                    return _db.Products.OrderBy(x => x.DateAdded).Skip(first).Take(last);
                }
            }

            return null;
        }

        public Product GetById(int productId)
        {
            return _db.Products.FirstOrDefault(x => x.ProductId == productId);
        }

        public bool RemoveById(int productId)
        {
            var result = GetById(productId);
            if (result != null)
            {
                _db.Products.Remove(result);
                _db.SaveChanges();
                return true;
            }

            return false;
        }

        public bool AddProduct(Product product, HttpPostedFileBase file)
        {
            throw new NotImplementedException();
        }

        public bool EditProduct(int productId, Product product)
        {
            throw new NotImplementedException();
        }

        public bool CheckSame(Product product)
        {
            throw new NotImplementedException();
        }

        public string WriteImage(HttpPostedFileBase file, string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> Find(Func<Product, bool> predicate)
        {
            return _db.Products.ToList().Where(predicate);
        }

        public bool ChangeImage(int productId, string path)
        {
            throw new NotImplementedException();
        }
    }
}