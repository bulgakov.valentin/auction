﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Contexts;
using Auction.DataAccessLayer.Entities.Shop;

namespace Auction.BusinessLogicLayer.AuctionData
{
    public class CategoryData : ICategory
    {
        private AuctionModels _db;

        public CategoryData(AuctionModels db)
        {
            _db = db;
        }

        public IEnumerable<Category> GetAll()
        {
            return _db.Categories.OrderBy(x => x.Name).ToList();
        }

        public IEnumerable<Category> GetRange(int first, int last)
        {
            int count = _db.Categories.Count();
            if (count >= last && first <= count && first >= 0 && last >= first)
            {
                var result = _db.Categories.OrderBy(x => x.Name).Skip(first).Take(last).ToList();
                return result;
            }
            else
            {
                if (first >= 0)
                {
                    last = last - (count - first);
                    return _db.Categories.OrderBy(x => x.Name).Skip(first).Take(last);
                }
            }

            return null;
        }

        public Category GetById(int categoryId)
        {
            return _db.Categories.FirstOrDefault(x => x.CategoryId == categoryId);
        }

        public bool RemoveById(int categoryId)
        {
            var result = GetById(categoryId);
            if (result != null)
            {
                _db.Categories.Remove(result);
                _db.SaveChanges();
                return true;
            }

            return false;
        }

        public bool CheckSame(Category category)
        {
            if (category != null)
            {
                var result = GetAll().Where(x => x.Name == category.Name
                                                 && x.Description == category.Description
                                                 && x.ShortDescription == category.ShortDescription
                                                 && x.ImageUrl == category.ImageUrl);
                if (!result.Any())
                {
                    return true;
                }
            }

            return false;
        }

        public bool AddCategory(Category category, HttpPostedFileBase file)
        {
            if (category != null && CheckSame(category))
            {
                category.ImageUrl = WriteImage(file, category.Name);
                _db.Categories.Add(category);
                _db.SaveChanges();
                return true;
            }
            return false;
        }

        public string WriteImage(HttpPostedFileBase file, string name)
        {
            if (file != null)
            {
                string extension = System.IO.Path.GetExtension(file.FileName);
                string fileName =
                    $"{name}day{DateTime.Now.Day}sec{DateTime.Now.Second}mil{DateTime.Now.Millisecond}{extension}";
                string imageUrl = "/DataHelpers/CategoryImages/" + fileName;
                string fullPath = HttpContext.Current.Server.MapPath("~" + imageUrl);
                file.SaveAs(fullPath);
                return imageUrl;
            }
            else
            {
                return "/DataHelpers/ImageHelper/null.png";
            }
        }

        public bool EditCategory(int categoryId, Category category)
        {
            var result = GetById(categoryId);
            if (result != null && CheckSame(category))
            {
                result.Name = category.Name;
                result.Description = category.Description;
                result.ShortDescription = category.ShortDescription;
                result.ImageUrl = category.ImageUrl;
                _db.SaveChanges();
                return true;
            }

            return false;
        }

        public IEnumerable<Category> Find(Func<Category, bool> predicate)
        {
            return _db.Categories.ToList().Where(predicate);
        }

        public bool ChangeImage(int categoryId, string path)
        {
            var result = GetById(categoryId);
            if (result != null && path != null)
            {
                result.ImageUrl = path;
                _db.SaveChanges();
                return true;
            }

            return false;
        }
    }
}