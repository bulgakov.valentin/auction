namespace Auction.DataAccessLayer.Migrations.AuctionContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ConfigurationAuction : DbMigrationsConfiguration<Auction.DataAccessLayer.Entities.Contexts.AuctionModels>
    {
        public ConfigurationAuction()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\AuctionContext";
            ContextKey = "Auction.DataAccessLayer.Entities.Contexts.AuctionModels";
        }

        protected override void Seed(Auction.DataAccessLayer.Entities.Contexts.AuctionModels context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
