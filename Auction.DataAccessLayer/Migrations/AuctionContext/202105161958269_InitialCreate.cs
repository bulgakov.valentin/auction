namespace Auction.DataAccessLayer.Migrations.AuctionContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        ImageUrl = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        ShortDescription = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.SubCategories",
                c => new
                    {
                        SubCategoryId = c.Int(nullable: false, identity: true),
                        ImageUrl = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        ShortDescription = c.String(),
                        CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.SubCategoryId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Features",
                c => new
                    {
                        FeatureId = c.Int(nullable: false, identity: true),
                        FeatureKey = c.String(),
                        FeatureValue = c.String(),
                    })
                .PrimaryKey(t => t.FeatureId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Count = c.String(),
                        DateAdded = c.DateTime(nullable: false),
                        Location = c.String(),
                        StartingPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SellerId = c.String(),
                        BuyerId = c.String(),
                        SubCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.SubCategories", t => t.SubCategoryId, cascadeDelete: true)
                .Index(t => t.SubCategoryId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductFeatures",
                c => new
                    {
                        Product_ProductId = c.Int(nullable: false),
                        Feature_FeatureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Product_ProductId, t.Feature_FeatureId })
                .ForeignKey("dbo.Products", t => t.Product_ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Features", t => t.Feature_FeatureId, cascadeDelete: true)
                .Index(t => t.Product_ProductId)
                .Index(t => t.Feature_FeatureId);
            
            CreateTable(
                "dbo.FeatureSubCategories",
                c => new
                    {
                        Feature_FeatureId = c.Int(nullable: false),
                        SubCategory_SubCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Feature_FeatureId, t.SubCategory_SubCategoryId })
                .ForeignKey("dbo.Features", t => t.Feature_FeatureId, cascadeDelete: true)
                .ForeignKey("dbo.SubCategories", t => t.SubCategory_SubCategoryId, cascadeDelete: true)
                .Index(t => t.Feature_FeatureId)
                .Index(t => t.SubCategory_SubCategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeatureSubCategories", "SubCategory_SubCategoryId", "dbo.SubCategories");
            DropForeignKey("dbo.FeatureSubCategories", "Feature_FeatureId", "dbo.Features");
            DropForeignKey("dbo.Products", "SubCategoryId", "dbo.SubCategories");
            DropForeignKey("dbo.Images", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductFeatures", "Feature_FeatureId", "dbo.Features");
            DropForeignKey("dbo.ProductFeatures", "Product_ProductId", "dbo.Products");
            DropForeignKey("dbo.SubCategories", "CategoryId", "dbo.Categories");
            DropIndex("dbo.FeatureSubCategories", new[] { "SubCategory_SubCategoryId" });
            DropIndex("dbo.FeatureSubCategories", new[] { "Feature_FeatureId" });
            DropIndex("dbo.ProductFeatures", new[] { "Feature_FeatureId" });
            DropIndex("dbo.ProductFeatures", new[] { "Product_ProductId" });
            DropIndex("dbo.Images", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "SubCategoryId" });
            DropIndex("dbo.SubCategories", new[] { "CategoryId" });
            DropTable("dbo.FeatureSubCategories");
            DropTable("dbo.ProductFeatures");
            DropTable("dbo.Images");
            DropTable("dbo.Products");
            DropTable("dbo.Features");
            DropTable("dbo.SubCategories");
            DropTable("dbo.Categories");
        }
    }
}
