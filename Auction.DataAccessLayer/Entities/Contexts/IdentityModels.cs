﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using Auction.DataAccessLayer.Entities.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Auction.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string LastName { get; set; }
        public string Firstname { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
    }

    public class UsersDbContext : IdentityDbContext<ApplicationUser>
    {
        public UsersDbContext(string connectionString)
            : base(connectionString)
        {
        }

        static UsersDbContext()
        {
            Database.SetInitializer<UsersDbContext>(new IdentityDbInit());
        }

        public static UsersDbContext Create()
        {
            return new UsersDbContext(connectionString: "IdentityForAuction");
        }
    }

    public class IdentityContextFactory : IDbContextFactory<UsersDbContext>
    {
        public UsersDbContext Create()
        {
            return new UsersDbContext("IdentityForAuction");
        }
    }
    public class IdentityDbInit : CreateDatabaseIfNotExists<UsersDbContext>
    {
        protected override void Seed(UsersDbContext context)
        {
            PerformInitialSetup(context);
        }

        public void PerformInitialSetup(UsersDbContext context)
        {
            AppUserManager userManager = new AppUserManager(new UserStore<ApplicationUser>(context));
            AppRoleManager roleManager = new AppRoleManager(new RoleStore<ApplicationRole>(context));
            string roleNameAdministrator = "Administrators";
            string roleNameUser = "Users";
            string roleNameModerator = "Managers";
            string userName = "Admin";
            string password = "admin12345";
            string email = "valentin@gmail.com";

            if (!roleManager.RoleExists(roleNameAdministrator))
            {
                roleManager.Create(new ApplicationRole(roleNameAdministrator));
            }

            if (!roleManager.RoleExists(roleNameUser))
            {
                roleManager.Create(new ApplicationRole(roleNameUser));
            }

            if (!roleManager.RoleExists(roleNameModerator))
            {
                roleManager.Create(new ApplicationRole(roleNameModerator));
            }

            ApplicationUser user = userManager.FindByName(userName);
            if (user == null)
            {
                var result = userManager.Create(new ApplicationUser { UserName = userName, Email = email }, password);
                user = userManager.FindByName(userName);
                if (!result.Succeeded)
                {
                    throw new Exception("The default administrator settings are configured incorrectly");
                }
            }
            if (!userManager.IsInRole(user.Id, roleNameAdministrator))
            {
                userManager.AddToRole(user.Id, roleNameAdministrator);
            }
        }
    }
}