﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Auction.DataAccessLayer.Entities.Shop;
using Auction.Models;

namespace Auction.DataAccessLayer.Entities.Contexts
{
    public class AuctionModels : DbContext
    {
        static AuctionModels()
        {
            Database.SetInitializer<AuctionModels>(new AuctionInitializer());
        }

        public AuctionModels(string connectionString) : base(connectionString)
        {
            
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Image> Images { get; set; }
    }
    //Enable-Migrations -MigrationsDirectory "Migrations\AuctionContext" -ContextTypeName Auction.DataAccessLayer.Entities.Contexts.AuctionModels
    //Add-Migration -ConfigurationTypeName ConfigA
    //Update-Database -ConfigurationTypeName ConfigA

    public class IdentityContextFactory : IDbContextFactory<AuctionModels>
    {
        public AuctionModels Create()
        {
            return new AuctionModels("AuctionDb");
        }
    }

    class AuctionInitializer : CreateDatabaseIfNotExists<AuctionModels>
    {
        protected override void Seed(AuctionModels db)
        {
            Category category = new Category()
            {
                Description = "description",
                ShortDescription = "short description",
                ImageUrl = null,
                Name = "Category"
            };
            db.Categories.Add(category);
            db.SaveChanges();
        }
    }
}
