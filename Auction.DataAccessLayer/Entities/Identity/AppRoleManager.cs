﻿using System;
using Auction.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Auction.DataAccessLayer.Entities.Identity
{
    public class AppRoleManager :RoleManager<ApplicationRole>, IDisposable
    {
        public AppRoleManager(IRoleStore<ApplicationRole, string> store) : base(store)
        {
        }
        public static AppRoleManager Create(
            IdentityFactoryOptions<AppRoleManager> options,
            IOwinContext context)
        {
            return new AppRoleManager(new
                RoleStore<ApplicationRole>(context.Get<UsersDbContext>()));
        }
    }
}
