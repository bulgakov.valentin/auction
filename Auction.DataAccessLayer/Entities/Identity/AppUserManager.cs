﻿using System;
using Auction.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Auction.DataAccessLayer.Entities.Identity
{

    public class AppUserManager : UserManager<ApplicationUser>, IDisposable
    {
        public AppUserManager(IUserStore<ApplicationUser> store) : base(store)
        {
        }
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options,
            IOwinContext context)
        {
            UsersDbContext db = context.Get<UsersDbContext>();
            AppUserManager manager = new AppUserManager(new UserStore<ApplicationUser>(db));
            return manager;
        }
    }
}
