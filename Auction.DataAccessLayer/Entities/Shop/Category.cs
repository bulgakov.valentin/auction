﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Auction.DataAccessLayer.Entities.Shop
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}
