﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auction.DataAccessLayer.Entities.Shop
{
    public class Feature
    {
        [Key]
        public int FeatureId { get; set; }
        public string FeatureKey { get; set; }
        public string FeatureValue { get; set; }
        public virtual ICollection<SubCategory> SubCategories { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
