﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Auction.DataAccessLayer.Entities.Shop
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Count { get; set; }
        public DateTime DateAdded { get; set; }
        public string Location { get; set; }
        public decimal StartingPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public string SellerId { get; set; }
        public string BuyerId { get; set; }
        public int SubCategoryId { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Feature> Features { get; set; }

    }
}
