﻿using System.ComponentModel.DataAnnotations;

namespace Auction.ViewModels.Identity
{
    public class EditViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        [StringLength(25, ErrorMessage = "Last Name must be less than 25 characters")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(25, ErrorMessage = "First Name must be less than 25 characters")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Nickname")]
        [StringLength(25, ErrorMessage = "Nickname must be less than 25 characters")]

        public string UserName { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
