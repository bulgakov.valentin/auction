﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Auction.ViewModels.Auction
{
    public class CreateSubCategoryViewModel
    {
        [Required]
        [Display(Name = "Sub category name")]
        [StringLength(50, ErrorMessage = "Sub category name must be less than 50 characters")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "Sub category description must be less than 2000 characters")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Short description")]
        [StringLength(700, ErrorMessage = "Sub category short description must be less than 700 characters")]
        public string ShortDescription { get; set; }
        [Display(Name = "Choose image")]
        public HttpPostedFileBase File { get; set; }

        /*[Required]
        [Display(Name = "Choose parent category")]
        public IEnumerable<SelectListItem> CategoriesList { get; set; }*/
        [Required]
        [Display(Name = "Choose parent category")]
        public int CategoryId { get; set; }
    }
}