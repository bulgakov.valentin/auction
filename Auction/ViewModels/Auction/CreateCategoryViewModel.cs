﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Auction.ViewModels.Auction
{
    public class CreateCategoryViewModel
    {
        [Required]
        [Display(Name = "Category name")]
        [StringLength(50, ErrorMessage = "Category name must be less than 50 characters")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "Category description must be less than 2000 characters")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Short description")]
        [StringLength(700, ErrorMessage = "Category short description must be less than 700 characters")]
        public string ShortDescription { get; set; }
        [Display(Name = "Choose image")]
        public HttpPostedFileBase File { get; set; }
    }
}