using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Auction.ViewModels.Auction
{
    public class EditCategoryViewModel
    {
        [Display(Name = "Choose image")]
        public HttpPostedFileBase File { get; set; }
        
        [Required]
        [Display(Name = "Category name")]
        [StringLength(50, ErrorMessage = "Category name must be less than 50 characters")]
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "Category description must be less than 2000 characters")]
        public string Description { get; set; }
        
        [Required]
        [Display(Name = "Short description")]
        [StringLength(700, ErrorMessage = "Category short description must be less than 700 characters")]
        public string ShortDescription { get; set; }
        
        public string ImageUrl { get; set; }
        
        public int Id { get; set; }
    }
}