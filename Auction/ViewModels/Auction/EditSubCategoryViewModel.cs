using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Auction.ViewModels.Auction
{
    public class EditSubCategoryViewModel
    {
        [Display(Name = "Choose image")]
        public HttpPostedFileBase File { get; set; }
        
        [Required]
        [Display(Name = "Sub category name")]
        [StringLength(50, ErrorMessage = "Sub category name must be less than 50 characters")]
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "Sub category description must be less than 2000 characters")]
        public string Description { get; set; }
        
        [Required]
        [Display(Name = "Short description")]
        [StringLength(700, ErrorMessage = "Sub category short description must be less than 700 characters")]
        public string ShortDescription { get; set; }
        
        public string ImageUrl { get; set; }
        
        public int Id { get; set; }
    }
}