﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Auction.BusinessLogicLayer.Identity;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Identity;
using Auction.Models;
using Auction.ViewModels.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Ninject;

namespace Auction.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class UsersController : Controller
    {
        private IUnitOfWorkIdentity repository;
        public UsersController(IUnitOfWorkIdentity unitOfWork)
        {
            repository = unitOfWork;
        }
        public ActionResult Index()
        {
            var users = repository.Users.GetAll().ToList();
            return View(users);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            bool result = repository.Users.Delete(id);
            if (result==true)
            {
                return RedirectToAction("Index", "Users");
            }
            else
            {
                return View("Error", new string[] { "User is not found" });
            }
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    LastName = model.LastName,
                    Firstname = model.FirstName
                };
                var result = await repository.Users.Create(user, model.Password);
                if (result==true)
                {
                    await repository.Users.UserManager.AddToRoleAsync(user.Id, "Users");
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        public async Task<ActionResult> Edit(string id)
        {
            ApplicationUser user = await repository.Users.UserManager.FindByIdAsync(id);
            if (user != null)
            {
                return View(user);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(string Id, string Email, string UserName, string FirstName, string LastName, string Password)
        {
            ApplicationUser user = await repository.Users.UserManager.FindByIdAsync(Id);
            if (user != null)
            {
                user.Email = Email;
                user.UserName = UserName;
                user.Firstname = FirstName;
                user.LastName = LastName;
                IdentityResult validEmail
                    = await repository.Users.UserManager.UserValidator.ValidateAsync(user);

                if (!validEmail.Succeeded)
                {
                    AddErrorsFromResult(validEmail);
                }

                IdentityResult validPass = null;
                if (Password != string.Empty)
                {
                    validPass
                        = await repository.Users.UserManager.PasswordValidator.ValidateAsync(Password);

                    if (validPass.Succeeded)
                    {
                        user.PasswordHash =
                            repository.Users.UserManager.PasswordHasher.HashPassword(Password);
                    }
                    else
                    {
                        AddErrorsFromResult(validPass);
                    }
                }

                if ((validEmail.Succeeded && validPass == null) ||
                    (validEmail.Succeeded && Password != string.Empty && validPass.Succeeded))
                {
                    IdentityResult result = await repository.Users.UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        repository.Save();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        AddErrorsFromResult(result);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", @"User is not found");
            }
            return View(user);
        }
        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}