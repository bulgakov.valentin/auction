﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Auction.BusinessLogicLayer.Identity;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Identity;
using Auction.Models;
using Auction.ViewModels.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Auction.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class RolesController : Controller
    {
        private IUnitOfWorkIdentity repository;
        public RolesController(IUnitOfWorkIdentity unitOfWork)
        {
            repository = unitOfWork;
        }
        // GET: Administrator/Roles
        public ActionResult Index()
        {
            var result = repository.Roles.GetAll();
            return View(result);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            bool result = await repository.Roles.Delete(id);
            if (result == true)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View("Error", new string[] { "Role not found" });
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create([Required] string name)
        {
            if (ModelState.IsValid)
            {
                ApplicationRole role = new ApplicationRole(name);
                bool result = await repository.Roles.Create(role);
                if (result==true)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error", new string[] { "Role not created" });
                }
            }
            return View(name);
        }

        public async Task<ActionResult> Edit(string id)
        {
            ApplicationRole role = await repository.Roles.Get(id);
            var membersIDs = role.Users.Select(x => x.UserId).ToArray();
            IEnumerable<ApplicationUser> members = repository.Users.Find(x => membersIDs.Any(y => y == x.Id));
            IEnumerable<ApplicationUser> nonMembers = repository.Users.GetAll().Except(members);
            return View(new RoleEditModel
            {
                Role = role,
                Members = members,
                NonMembers = nonMembers
            });
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleModificationModel model)
        {
            IdentityResult result;
            if (ModelState.IsValid)
            {
                foreach (string userId in model.IdsToAdd ?? new string[] { })
                {
                    result = await repository.Roles.UserManager.AddToRoleAsync(userId, model.RoleName);

                    if (!result.Succeeded)
                    {
                        return View("Error", result.Errors);
                    }
                }
                foreach (string userId in model.IdsToDelete ?? new string[] { })
                {
                    result = await repository.Roles.UserManager.RemoveFromRoleAsync(userId,
                        model.RoleName);

                    if (!result.Succeeded)
                    {
                        return View("Error", result.Errors);
                    }
                }
                return RedirectToAction("Index");

            }
            return View("Error", new string[] { "Role not found" });
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}