using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Shop;
using Auction.ViewModels.Auction;

namespace Auction.Areas.Manager.Controllers
{
    [Authorize(Roles = "Administrators, Managers")]
    public class SubCategoryController : Controller
    {
        private readonly IUnitOfWorkAuction _repository;
        public SubCategoryController(IUnitOfWorkAuction unitOfWork)
        {
            _repository = unitOfWork;
        }
        public ActionResult Index()
        {
            SelectList categories = new SelectList(_repository.Categories.GetAll(), "Name", "Name");
            var selectListItems = categories.Append(new SelectListItem(){Value = @"All", Text = @"All"}).OrderBy(x => x.Text);
            ViewBag.Categories = selectListItems;
            
            return View();
        }

        [HttpPost]
        public ActionResult SubCategorySearch(string name = "All")
        {
            ViewBag.CategoryName = name;
            var result = new List<SubCategory>();
            if (name == "All")
            {
                result = _repository.SubCategories.GetAll().ToList();
            }
            else
            {
                result.AddRange(_repository.Categories.GetAll().Where(category => category.Name == name).SelectMany(category => category.SubCategories));
            }
            ViewBag.SubCategories = result;
            return PartialView();
        }
        [HttpGet]
        public ActionResult Edit(int subcategoryId)
        {
            var subCategory = _repository.SubCategories.GetById(subcategoryId);
            if (subCategory!=null)
            {
                ViewBag.SubCategory = subCategory;
                return View();
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(EditSubCategoryViewModel model)
        {
            var subCategory = _repository.SubCategories.GetById(model.Id);
            ViewBag.SubCategory = subCategory;
            if (ModelState.IsValid)
            {
                var current = new SubCategory()
                {
                    Name = model.Name, Description = model.Description, ShortDescription = model.ShortDescription
                };
                if (model.File==null)
                {
                    current.ImageUrl = model.ImageUrl;
                    _repository.SubCategories.EditSubCategory(model.Id, current);
                }
                else
                {
                    current.ImageUrl = _repository.SubCategories.WriteImage(model.File, model.Name);
                    _repository.SubCategories.EditSubCategory(model.Id, current);
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int subcategoryId)
        {
            var result = _repository.SubCategories.RemoveById(subcategoryId);
            if (result)
            {
                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }

        public ActionResult Create()
        {
            SelectList categories = new SelectList(_repository.Categories.GetAll(), "CategoryId", "Name");
            ViewBag.Categories = categories;
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(CreateSubCategoryViewModel model)
        {
            SelectList categories = new SelectList(_repository.Categories.GetAll(), "CategoryId", "Name");
            ViewBag.Categories = categories;
            if (ModelState.IsValid)
            {
                /*int categoryId = _repository.Categories.Find(x => x.Name == model.CategoryId.ToString()).First().CategoryId;*/
                SubCategory subCategory = new SubCategory
                {
                    Name = model.Name, Description = model.Description, ShortDescription = model.ShortDescription, CategoryId = model.CategoryId
                };
                var result = _repository.SubCategories.AddSubCategory(subCategory, model.File);
                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
    }
}