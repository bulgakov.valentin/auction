﻿using System.Linq;
using System.Web.Mvc;
using Auction.BusinessLogicLayer.Interfaces;
using Auction.DataAccessLayer.Entities.Shop;
using Auction.ViewModels.Auction;

namespace Auction.Areas.Manager.Controllers
{
    [Authorize(Roles = "Administrators, Managers")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWorkAuction _repository;
        public CategoryController(IUnitOfWorkAuction unitOfWork)
        {
            _repository = unitOfWork;
        }
        public ActionResult Index()
        {
            var result = _repository.Categories.GetAll().ToList();
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(CreateCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category
                {
                    Name = model.Name, Description = model.Description, ShortDescription = model.ShortDescription
                };
                var result = _repository.Categories.AddCategory(category, model.File);
                if (result)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int categoryId)
        {
            var category = _repository.Categories.GetById(categoryId);
            if (category!=null)
            {
                ViewBag.Category = category;
                return View();
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(EditCategoryViewModel model)
        {
            var category = _repository.Categories.GetById(model.Id);
            ViewBag.Category = category;
            if (ModelState.IsValid)
            {
                var current = new Category
                {
                    Name = model.Name, Description = model.Description, ShortDescription = model.ShortDescription
                };
                if (model.File==null)
                {
                    current.ImageUrl = model.ImageUrl;
                    _repository.Categories.EditCategory(model.Id, current);
                }
                else
                {
                    current.ImageUrl = _repository.Categories.WriteImage(model.File, model.Name);
                    _repository.Categories.EditCategory(model.Id, current);
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }
        
        public ActionResult Delete(int categoryId)
        {
            var result = _repository.Categories.RemoveById(categoryId);
            if (result)
            {
                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }
    }
}