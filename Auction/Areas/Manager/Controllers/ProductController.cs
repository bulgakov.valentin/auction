using System.Web.Mvc;

namespace Auction.Areas.Manager.Controllers
{
    [Authorize(Roles = "Administrators, Managers")]
    public class ProductController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductsSearch()
        {
            throw new System.NotImplementedException();
        }

        public ActionResult Create()
        {
            throw new System.NotImplementedException();
        }
    }
}