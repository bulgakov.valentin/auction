﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Auction.BusinessLogicLayer.Util;
//using Auction.DataAccessLayer.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;

namespace Auction
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Database.SetInitializer<UsersDbContext>(new IdentityDbInit());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<UsersDbContext>());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule registrations = new NinjectRegistrations();
            var kernel = new StandardKernel(registrations);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
